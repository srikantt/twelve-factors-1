# Twelve Factors 1

This is the part of the Developing & Testing Software-as-a-Service Applications course.

## Introduction

This course demonstrates the [Twelve-Factor App](https://12factor.net/) methodology.

This course can be run on a Windows, Linux or MacOs machine.

## Prerequisites

node, npm, git, and curl are installed.

A Gitlab or GitHub account. See below. Please select the free tier.


## First task - codebase

Please see the sub-directory codebase and follow the steps in the README.md contained in that sub-directory.

This tasks covers two factors:-
I. Codebase (One codebase tracked in revision control, many deploys) and 
II. Dependencies (Explicitly declare and isolate dependencies).

## Installation requirements.


### Node and npm
 
[Node and npm installation instructions](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm#using-a-node-version-manager-to-install-node-js-and-npm)

Use nvm (node version manager) to manage your node and npm installations.
 
A simple example of using node and npm is given below. This is not necessary for the course. It is provided for future reference.
 
[Simple application using node and npm](https://expressjs.com/en/starter/hello-world.html)
 

### git

Please install git.
 
[install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
 
 
### curl

Check you can use the command curl on your machine. It may already be available.

[curl installation instructions](https://everything.curl.dev/get)
 
[Windows technical information note](https://techcommunity.microsoft.com/t5/containers/tar-and-curl-come-to-windows/ba-p/382409)

Try out curl

```bash
 curl --url date.jsontest.com    

```

### Gitlab signup
[Gitlab signup](https://gitlab.com/users/sign_up)



### Trouble Shooting
In some cases a command may require administrator (root) privileges.

Put sudo in front of the command, and enter your password when prompted.
